the code was compiled on linux using cmake

the program will look for the "image_set" folder.
the image_set folder should be in the parent directory of where 
the program is being executed.
and look for the images that contain the "mosaic" key word

when prompted input the number of the desired image the appears on screen to load an image
leaving the propt blank and pressing enter will load the crayons_mosaic.bmp file

the assignment required us to demoaic an image and 
interpolate the color image from the resulting image channels.

The reason for the artifacts in the square difference image is due to the 
interpolation error when filtering the red, blue, and green channels.
one can notice that there is less error in the red channel because there
was more red data to begin with.

In the second part of the assignment the difference image has slightly less 
articating due to the new information introduced by the red channel when computing
the G-R and B-R images. 
