//
// Created by Anthony on 1/7/2020.
//
#include <memory>
#include <tuple>
#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <iostream>
#include <filesystem>
#include <sstream>
#define DEFAULT_IMAGE "../image_set/crayons_mosaic.bmp"
#define IMAGE_DIRECTORY "../image_set"
#ifdef __linux__
    #define CV_LOAD_IMAGE_COLOR IMREAD_COLOR
    #define CV_LOAD_IMAGE_GRAYSCALE IMREAD_GRAYSCALE

#endif

using namespace cv;
using namespace std;
//namespace fs = std::filesystem;

/*
 * returns the type of the image as a string
 * */
string type2str(int type);
/*
 * returns a tuple of the split blue, green and red channels from the mosaiced image
 * */
tuple<Mat,Mat,Mat> DemosaicImage(const Mat& image);
/*
 * computes an image from the square difference of the computed image and the given image
 * */
Mat squaredifference(const Mat& computedImage, const Mat& originalImage);
/*
 * Adjusts other channels based on the red channel because it is sampled at twice the rate
 * */
Mat RedChannelInterpolation(const Mat& image);


int main(int argc, char **argv)
{

    cout<<"====================================\nCOMP425 - Bayer to RGB image converter\n===================================="<<endl;



    std::string path = IMAGE_DIRECTORY;
    vector<string> _image_paths;
    int i=0;
    for (const auto & entry : std::filesystem::directory_iterator(path)){
        if(entry.path().string().find("mosaic")!=string::npos){
            _image_paths.push_back(entry.path());
            std::cout <<"["<< i<<"]" <<entry.path() << std::endl;
            ++i;
        }

    }
    cout<<"\nimage to import (If blank will use default image) : ";
    string filename;
    string selection;
    string comparisonfile;
    getline(std::cin, selection);
    if(selection.empty()){
        filename = DEFAULT_IMAGE;
    }else {

        istringstream(selection)>>i;
        if(i<_image_paths.size()) filename = _image_paths[i];
    }

    comparisonfile = filename.substr(0,filename.find_last_of('/')+1)+ filename.substr(filename.find_last_of('/')+1,(filename.find_last_of('_')-filename.find_last_of('/'))-1) + ".jpg";

    string _channel_input;
    bool _view_channels;
    cout<<"\noutput separated channels(default = false)? ";
    getline(std::cin, _channel_input);
    _channel_input == "true" || _channel_input == "yes" ? _view_channels = true : _view_channels = false;

    Mat image;
    //flags : 0 = greyscale
    //flags : 1 = color
    image = imread(filename,CV_LOAD_IMAGE_GRAYSCALE);
    //image = imread(filename,0);
    Mat comparisonimage = imread(comparisonfile, 1);


    if(! image.data )                                       // Check for invalid input
    {
        cout <<  "Could not open or find the image " << filename<<std::endl ;
        return -1;
    }
    if(!comparisonimage.data){

        cout <<  "Could not open or find the image " << comparisonfile<<std::endl ;
        return -1;

    }


    if ( ! image.isContinuous() )
    {
        image = image.clone();
    }


    string imagename = filename.substr(filename.find_last_of('/')+1);
    cout<<"\nnumber of channels in "<<imagename <<": "<<image.channels()<<endl;
    string ty =  type2str( image.type() );
    printf("Matrix: %s %dx%d \n", ty.c_str(), image.cols, image.rows );

    auto _separated_channels = DemosaicImage(image);

    Mat Blue_filtered_data;
    Mat Green_filtered_data;
    Mat Red_filtered_data;


    /*
     *
     * make kernels
     * */
    Mat_<float> red_kernel(3,3);
    red_kernel<< 0, 0.25, 0,
                 0.25, 1, 0.25,
                 0, 0.25, 0;



    Mat_<float> bg_kernel(3,3);
    bg_kernel<< 0.25, 0, 0.25,
                0, 1, 0,
                0.25, 0, 0.25;

    /*
     * filter image
     * apply kernel with filter2d
     * */
    get<0>(_separated_channels).convertTo(get<0>(_separated_channels),CV_64F);
    get<1>(_separated_channels).convertTo(get<1>(_separated_channels),CV_64F);
    get<2>(_separated_channels).convertTo(get<2>(_separated_channels),CV_64F);


    filter2D(get<0>(_separated_channels), Blue_filtered_data, -1, bg_kernel);
    filter2D(get<1>(_separated_channels), Green_filtered_data, -1, bg_kernel);
    filter2D(Blue_filtered_data, Blue_filtered_data, -1, red_kernel);
    filter2D(Green_filtered_data, Green_filtered_data, -1, red_kernel);
    filter2D(get<2>(_separated_channels), Red_filtered_data, -1, red_kernel);


    Blue_filtered_data.convertTo(Blue_filtered_data,image.type());
    Green_filtered_data.convertTo(Green_filtered_data,image.type());
    Red_filtered_data.convertTo(Red_filtered_data,image.type());


    auto channels = vector<Mat>{Blue_filtered_data, Green_filtered_data, Red_filtered_data  };

    Mat BGR;
    merge(channels, BGR);

    Mat sqrdiff = squaredifference(BGR,comparisonimage);

    Mat interpolated_BGR = RedChannelInterpolation(BGR);

    Mat sqrdiff_interpolated = squaredifference(interpolated_BGR,comparisonimage);

    namedWindow( "Input Image", WINDOW_AUTOSIZE );       // Create a window for display.
    imshow( "Input Image", image );                      // Show our image inside it.

    namedWindow( "Combined Images(part1)", WINDOW_AUTOSIZE );       // Create a window for display.
    imshow("Combined Images(part1)", BGR );                               // Show our image inside it.

    namedWindow( "Original Image", WINDOW_AUTOSIZE );       // Create a window for display.
    imshow("Original Image", comparisonimage );                   // Show our image inside it.

    namedWindow( "Difference Image(part1)", WINDOW_AUTOSIZE );       // Create a window for display.
    imshow("Difference Image(part1)", sqrdiff );                           // Show our image inside it.

    namedWindow( "Part 2", WINDOW_AUTOSIZE );
    imshow("Part 2", interpolated_BGR );

    namedWindow( "Part 2 Difference Image", WINDOW_AUTOSIZE );
    imshow("Part 2 Difference Image", sqrdiff_interpolated );

    if(_view_channels){
        Mat B;
        Mat G;
        Mat R;
        Mat zero = Mat::zeros(image.rows,image.cols,image.type());
        auto temp_b_img = vector<Mat>{Blue_filtered_data, zero, zero };
        auto temp_g_img = vector<Mat>{zero, Green_filtered_data, zero };
        auto temp_r_img = vector<Mat>{zero, zero, Red_filtered_data };
        merge(temp_b_img, B);
        merge(temp_g_img, G);
        merge(temp_r_img, R);
        namedWindow( "blue Images", WINDOW_AUTOSIZE );       // Create a window for display.
        imshow("blue Images", B );                               // Show our image inside it.

        namedWindow( "green Images", WINDOW_AUTOSIZE );       // Create a window for display.
        imshow("green Images", G );                               // Show our image inside it.

        namedWindow( "red Images", WINDOW_AUTOSIZE );       // Create a window for display.
        imshow("red Images", R );                               // Show our image inside it.
    }


    waitKey(0);

    destroyAllWindows();
// imwrite("imagename.png", Image)

    return 0;
}

string type2str(int type) {
    string r;

    uchar depth = type & CV_MAT_DEPTH_MASK;
    uchar chans = 1 + (type >> CV_CN_SHIFT);

    switch ( depth ) {
        case CV_8U:  r = "8U"; break;
        case CV_8S:  r = "8S"; break;
        case CV_16U: r = "16U"; break;
        case CV_16S: r = "16S"; break;
        case CV_32S: r = "32S"; break;
        case CV_32F: r = "32F"; break;
        case CV_64F: r = "64F"; break;
        default:     r = "User"; break;
    }

    r += "C";
    r += (chans+'0');

    return r;
}

tuple<Mat,Mat,Mat> DemosaicImage(const Mat& image){

    Mat bdata(image.rows,image.cols, image.type());
    Mat gdata(image.rows,image.cols, image.type());
    Mat rdata(image.rows,image.cols, image.type());


    /*
     * if the col is even and row even then B
     * if the col is even and row is odd then R
     * if the col is odd and row is even then R
     * if the col is odd and row is odd the G
     * */
    bool reven = false;
    bool ceven = false;
    for(int i = 0; i < image.rows; i++){
        reven  = (i%2 == 0);
        for(int j = 0; j < image.cols; j++){

            ceven = (j%2 == 0);

            if(ceven && reven){
                bdata.at<uchar>(i,j) = image.at<uchar>(i,j);
            }else if (ceven && !reven){
                rdata.at<uchar>(i,j) = image.at<uchar>(i,j);
            } else if (!ceven && reven){
                rdata.at<uchar>(i,j) = image.at<uchar>(i,j);
            }else if(!ceven && !reven){
                gdata.at<uchar>(i,j) = image.at<uchar>(i,j);
            }

        }
    }

    return std::make_tuple(bdata,gdata,rdata);
}

Mat squaredifference(const Mat& computedImage, const Mat& originalImage){

    vector<Mat>channel_comparison;

    vector<Mat> original_channels;
    split(originalImage,original_channels);

    vector<Mat> computed_channels;
    split(computedImage,computed_channels);

    Mat bdata(originalImage.rows,originalImage.cols, originalImage.type());
    Mat gdata(originalImage.rows,originalImage.cols, originalImage.type());
    Mat rdata(originalImage.rows,originalImage.cols, originalImage.type());

    cv::subtract(computed_channels[0], original_channels[0],bdata);
    cv::subtract(computed_channels[1], original_channels[1],gdata);
    cv::subtract(computed_channels[2], original_channels[2],rdata);

    channel_comparison.push_back(bdata.mul(bdata));
    channel_comparison.push_back(gdata.mul(gdata));
    channel_comparison.push_back(rdata.mul(rdata));

    Mat comparison;
    merge(channel_comparison, comparison);

    return comparison;
}

Mat RedChannelInterpolation(const Mat& image){

    vector<Mat> BGR;
    split(image,BGR);

    BGR[0].convertTo(BGR[0],CV_64F);
    BGR[1].convertTo(BGR[1],CV_64F);
    BGR[2].convertTo(BGR[2],CV_64F);

    subtract(BGR[0],BGR[2],BGR[0]);
    subtract(BGR[1],BGR[2],BGR[1]);


    Mat median_kernel = Mat::ones(3,3,/*image.type()*/CV_32F)/(float)9;


    filter2D(BGR[0],BGR[0],-1,median_kernel);
    filter2D(BGR[1],BGR[1],-1,median_kernel);

    add(BGR[0],BGR[2],BGR[0]);
    add(BGR[1],BGR[2],BGR[1]);

    BGR[0].convertTo(BGR[0],image.type());
    BGR[1].convertTo(BGR[1],image.type());
    BGR[2].convertTo(BGR[2],image.type());

    Mat final;
    merge(BGR,final);

    return final;
}
